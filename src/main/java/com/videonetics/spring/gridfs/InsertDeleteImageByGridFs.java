package com.videonetics.spring.gridfs;

import java.io.File;
import java.io.IOException;

import com.mongodb.DB;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSInputFile;

public class InsertDeleteImageByGridFs {

	public static void main(String[] args) {
		
		
	}
	
	private static void saveImageIntoMongoDB(DB db) throws IOException {
	    String dbFileName = "DemoImage";
	    File imageFile = new File("c:\\DemoImage.png");
	    GridFS gfsPhoto = new GridFS(db, "photo");
	    GridFSInputFile gfsFile = gfsPhoto.createFile(imageFile);
	    gfsFile.setFilename(dbFileName);
	    gfsFile.save();
	}

}
