package com.videonetics.spring.gridoperations;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.bson.types.ObjectId;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.gridfs.GridFsOperations;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.gridfs.GridFSFile;

public class GridFsAppStore {
	
	public static void main(String[] args) {

		@SuppressWarnings("resource")
		ApplicationContext ctx = 
	                     new AnnotationConfigApplicationContext(SpringMongoConfig.class);
		GridFsOperations gridOperations = 
	                      (GridFsOperations) ctx.getBean("gridFsTemplate");

		DBObject metaData = new BasicDBObject();
		metaData.put("branchid", new ObjectId("55c079274a770afc0b004823"));
		metaData.put("channelid", 101);
		/*add alerttypeid for alert snapshort, add eventtypeid for event snapshort and do not set both for health snapshot*/   
		metaData.put("alerttypeid", new ObjectId("55c09f5b2ea27e08c1203847"));
		//metaData.put("eventtypeid", new ObjectId("55c1a1354a770a5c1100002e"));
		metaData.put("epoch", 1437554724783L);

		InputStream inputStream = null;
		try {
			inputStream = new FileInputStream("E:/images1.jpeg");
			GridFSFile gridFsFile = gridOperations.store(inputStream, "images1.jpeg", "image/jpeg", metaData);
			System.out.println("image file _id: " + gridFsFile.getId());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		metaData = new BasicDBObject();
		metaData.put("branchid", new ObjectId("55c079274a770afc0b004823"));
		metaData.put("channelid", 102);
		/*add alerttypeid for alert snapshort, add eventtypeid for event snapshort and do not set both for health snapshot*/
		metaData.put("alerttypeid", new ObjectId("55c09f5b2ea27e08c1203847"));
		//metaData.put("eventtypeid", new ObjectId("55c1a1354a770a5c1100002e"));
		metaData.put("epoch", 1437554724783L);

		inputStream = null;
		try {
			inputStream = new FileInputStream("E:/images2.jpeg");
			GridFSFile gridFsFile = gridOperations.store(inputStream, "images2.jpeg", "image/jpeg", metaData);
			System.out.println("image file _id: " + gridFsFile.getId());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

			System.out.println("Done");
	    }
}
