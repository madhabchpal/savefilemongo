package com.videonetics.spring.gridoperations;

import java.io.IOException;
import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;

import com.mongodb.gridfs.GridFSDBFile;

public class GridFsAppRead {
	
	public static void main(String[] args) {

		@SuppressWarnings("resource")
		ApplicationContext ctx = 
	              new ClassPathXmlApplicationContext("SpringConfig.xml");
		GridFsOperations gridOperations = 
	              (GridFsOperations) ctx.getBean("gridFsTemplate");

		List<GridFSDBFile> result = gridOperations.find(
	               new Query().addCriteria(Criteria.where("filename").is("images1.jpeg")));
		
		for (GridFSDBFile file : result) {
			try {
				System.out.println(file.getFilename());
				System.out.println(file.getContentType());
					
				//save as another image
				file.writeTo("E:/" + file.getFilename());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		GridFSDBFile file = gridOperations.findOne(new Query(Criteria.where("_id").is(new ObjectId("55c0c37b3cbd392ebafd866f"))));
		try {
			System.out.println(file.getFilename());
			System.out.println(file.getContentType());
				
			//save as another image
			file.writeTo("E:/" + file.getFilename());
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("Done");

	    }

}
